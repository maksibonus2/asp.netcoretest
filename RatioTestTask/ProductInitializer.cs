﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestTask.Model;

namespace TestTask
{
    public class ProductInitializer
    {
        #region Methods

        #region public
        public static void Initialize(ProductContext context, IServiceProvider services)
        {
            var isCreated = context.Database.EnsureCreated();

            if (isCreated)
            {
                CreateGroups(context);

                var products = GetProducts(context).ToList();
                var stores = GetStores(context).ToList();

                context.AddRange(
                    new StorageItem { Product = products[0], Store = stores[1] },
                    new StorageItem { Product = products[0], Store = stores[2] },
                    new StorageItem { Product = products[1], Store = stores[0] },
                    new StorageItem { Product = products[1], Store = stores[2] },
                    new StorageItem { Product = products[2], Store = stores[0] },
                    new StorageItem { Product = products[3], Store = stores[1] });

                context.SaveChanges();
            }          
        }
        #endregion

        #region private
        private static void CreateGroups(ProductContext context)
        {
            var groups = new List<Group>();

            var fruitsGroup = new Group()
            {
                Name = "Fruits"
            };
            context.Groups.Add(fruitsGroup);

            var citrusGroup = new Group()
            {
                Name = "Citrus",
                ParentId = 1
            };
            context.Groups.Add(citrusGroup);

            var berriesGroup = new Group()
            {
                Name = "Berries",
                ParentId = 1
            };
            context.Groups.Add(berriesGroup);

            var furnituresGroup = new Group()
            {
                Name = "Furniture"
            };
            context.Groups.Add(furnituresGroup);

            var chairsGroup = new Group()
            {
                Name = "Сhairs",
                ParentId = 4
            };
            context.Groups.Add(chairsGroup);

            var lockersGroup = new Group()
            {
                Name = "Lockers",
                ParentId = 4
            };
            context.Groups.Add(lockersGroup);

            context.SaveChanges();
        }

        private static IEnumerable<Product> GetProducts(ProductContext context)
        {
            var products = new List<Product>();

            var appleProduct = new Product()
            {
                Name = "Apple",
                Price = 4.5m,
                VATPrice = 5.4m,
                VATRate = 20,
                Group = context.Groups.FirstOrDefault(g => g.GroupId == 1),
                AddTime = new DateTime(2019, 02, 17, 08, 12, 15),
            };
            products.Add(appleProduct);

            var orangeProduct = new Product()
            {
                Name = "Orange",
                Price = 9m,
                VATPrice = 9.9m,
                VATRate = 10,
                Group = context.Groups.FirstOrDefault(g => g.GroupId == 2),
                AddTime = new DateTime(2019, 02, 16, 10, 02, 12),
            };
            products.Add(orangeProduct);

            var throneProduct = new Product()
            {
                Name = "Throne",
                Price = 99.9m,
                VATPrice = 112.887m,
                VATRate = 13,
                Group = context.Groups.FirstOrDefault(g => g.GroupId == 5),
                AddTime = new DateTime(2019, 02, 16, 15, 02, 12),
            };
            products.Add(throneProduct);

            var bigLockerProduct = new Product()
            {
                Name = "Big locker",
                Price = 150m,
                VATPrice = 240m,
                VATRate = 60,
                Group = context.Groups.FirstOrDefault(g => g.GroupId == 6),
                AddTime = new DateTime(2019, 02, 17, 19, 16, 30),
            };
            products.Add(bigLockerProduct);

            return products;
        }

        private static IEnumerable<Store> GetStores(ProductContext context)
        {
            var stores = new List<Store>();

            var placeStore = new Store()
            {
                Name = "All places",
            };
            stores.Add(placeStore);

            var bobbyStore = new Store()
            {
                Name = "Bobby's Department",
            };
            stores.Add(bobbyStore);

            var silpoStore = new Store()
            {
                Name = "Silpo",
            };
            stores.Add(silpoStore);

            return stores;
        }
        #endregion

        #endregion
    }
}