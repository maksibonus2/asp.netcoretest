﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TestTask.Model;

namespace TestTask.Message
{
    public class ProductMessageOutput
    {
        #region Properties

        public int ProductId { get; set; }

        public string Name { get; set; }

        public string GroupName { get; set; }

        public DateTime AddTime { get; set; }

        public decimal Price { get; set; }

        public decimal VATPrice { get; set; }

        public int VATRate { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<Store> Stores { get; set; }
        #endregion
    }
}
