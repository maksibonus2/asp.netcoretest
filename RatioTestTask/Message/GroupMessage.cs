﻿namespace TestTask.Message
{
    public class GroupMessage
    {
        #region Properties
        public int GroupId { get; set; }

        public string Name { get; set; }

        public int? ParentId { get; set; }

        public int[] ChildrenIds { get; set; }
        #endregion
    }
}
