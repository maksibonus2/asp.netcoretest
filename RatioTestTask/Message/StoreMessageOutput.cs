﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TestTask.Model;

namespace RatioTestTask.Message
{
    public class StoreMessageOutput
    {
        #region Properties
        public int StoreId { get; set; }

        public string Name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<Product> Products { get; set; }
        #endregion
    }
}
