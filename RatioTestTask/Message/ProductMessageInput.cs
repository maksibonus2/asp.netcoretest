﻿namespace TestTask.Message
{
    public class ProductMessageInput
    {
        #region Properties
        public string Name { get; set; }

        public string GroupName { get; set; }

        public decimal? Price { get; set; }

        public decimal? VATPrice { get; set; }

        public int? VATRate { get; set; }

        public int[] StoresIds { get; set; }
        #endregion
    }
}
