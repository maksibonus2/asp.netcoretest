﻿using TestTask.Model;

namespace TestTask.Services
{
    public class ServiceBase
    {
        #region Constructors
        public ServiceBase(ProductContext context)
        {
            this.Context = context;
        }
        #endregion

        #region Properties
        protected ProductContext Context { get; }
        #endregion
    }
}
