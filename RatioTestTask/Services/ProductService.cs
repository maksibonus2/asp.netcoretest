﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using TestTask.Message;
using TestTask.Model;
using TestTask.Services.Interfaces;

namespace TestTask.Services
{
    public class ProductService : ServiceBase, IProductService
    {
        #region Constructors
        public ProductService(ProductContext context) : base(context)
        { }
        #endregion

        #region Methods

        #region public
        public ModelStateDictionary TryAddProduct(ProductMessageInput productMessage)
        {
            var modelStateDictionary = this.VerifyProductMessage(productMessage);
            if (modelStateDictionary.ErrorCount != 0)
            {
                return modelStateDictionary;
            }

            this.CalculatePrice(productMessage);

            this.CreateNewProduct(productMessage);

            return null;
        }

        public IEnumerable<ProductMessageOutput> GetProducts()
        {
            var productMessages = new List<ProductMessageOutput>();
            var products = this.Context.Products.Include(p => p.Group).AsNoTracking();

            foreach (var product in products)
            {
                var productMessage = this.GetProductMessage(product);
                productMessages.Add(productMessage);
            }

            return productMessages;
        }

        public ProductMessageOutput TryGetProduct(int id)
        {
            var product = this.Context.Products.Include(p => p.Group)
                                               .AsNoTracking()
                                               .FirstOrDefault(p => p.ProductId == id);
            if (product == null)
            {
                return null;
            }

            var productMessage = this.GetProductMessage(product);
            return productMessage;
        }
        #endregion

        #region private

        private void CreateNewProduct(ProductMessageInput productMessage)
        {
            int groupId = this.GetGroupId(productMessage.GroupName);

            var now = DateTime.Now;
            var currentDate = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);

            Product product = new Product()
            {
                Name = productMessage.Name,
                GroupId = groupId,
                AddTime = currentDate,
                Price = productMessage.Price.Value,
                VATPrice = productMessage.VATPrice.Value,
                VATRate = productMessage.VATRate.Value
            };

            this.Context.Products.Add(product);
            this.Context.SaveChanges();

            var productId = product.ProductId;

            foreach (var storeId in productMessage.StoresIds)
            {
                var store = this.Context.Stores.AsNoTracking().FirstOrDefault(s => s.StoreId == storeId);
                if (store != null)
                {
                    var storageItem = new StorageItem()
                    {
                        ProductId = productId,
                        StoreId = storeId
                    };
                    product.StorageItems.Add(storageItem);
                }
            }

            this.Context.SaveChanges();
        }

        private int GetGroupId(string groupName)
        {
            var selectedGroup = this.Context.Groups.AsNoTracking().FirstOrDefault(g => g.Name == groupName);
            int groupId;

            if (selectedGroup == null)
            {
                var group = new Group();
                group.Name = groupName;

                this.Context.Groups.Add(group);
                this.Context.SaveChanges();

                groupId = group.GroupId;
            }
            else
            {
                groupId = selectedGroup.GroupId;
            }

            return groupId;
        }

        private ProductMessageOutput GetProductMessage(Product product)
        {
            var stores = (from store in this.Context.Stores
                          from storageItem in store.StorageItems.Where(x => x.ProductId == product.ProductId)
                          select store).AsNoTracking();

            var productMessage = new ProductMessageOutput()
            {
                ProductId = product.ProductId,
                AddTime = product.AddTime,
                Name = product.Name,
                GroupName = product.Group.Name,
                Price = product.Price,
                VATPrice = product.VATPrice,
                VATRate = product.VATRate,
                Stores = stores
            };

            return productMessage;
        }

        private ModelStateDictionary VerifyProductMessage(ProductMessageInput productMessage)
        {
            var modelStateDictionary = new ModelStateDictionary();

            if (string.IsNullOrEmpty(productMessage.Name))
            {
                modelStateDictionary.AddModelError("Name", "'Name' cannot has empty or null value");
            }

            if (string.IsNullOrEmpty(productMessage.GroupName))
            {
                modelStateDictionary.AddModelError("GroupName", "'GroupName' cannot has empty or null value");
            }
            
            if (!this.VerifyPrice(productMessage))
            {
                modelStateDictionary.AddModelError("Price, VARPrice, VATRate", "Two values must be not null");
            }

            if (productMessage.StoresIds == null || productMessage.StoresIds.Count() == 0)
            {
                modelStateDictionary.AddModelError("StoresIds", "You can add product to existing stores only once. Create more stores by using another request.");
            }

            return modelStateDictionary;
        }

        private bool VerifyPrice(ProductMessageInput productMessage)
        {
            int nullCounter = 0;
            if (productMessage.VATRate == null)
            {
                nullCounter++;
            }

            if (productMessage.VATPrice == null)
            {
                nullCounter++;
            }

            if (productMessage.Price == null)
            {
                nullCounter++;
            }

            if (nullCounter > 1)
            {
                return false;
            }

            return true;
        }

        private void CalculatePrice(ProductMessageInput productMessage)
        {
            if (productMessage.VATRate == null)
            {
                productMessage.VATRate = (int)(productMessage.VATPrice.Value / productMessage.Price.Value) * 100 - 100;
            }
            else if (productMessage.VATPrice == null)
            {
                productMessage.VATPrice = productMessage.Price + productMessage.Price * productMessage.VATRate / 100;
            }
            else if (productMessage.Price == null)
            {
                productMessage.Price = 100 * productMessage.VATPrice / (100 + productMessage.VATRate);
            }
        }
        #endregion

        #endregion
    }
}
