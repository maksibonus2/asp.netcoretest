﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using RatioTestTask.Message;
using RatioTestTask.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using TestTask.Model;
using TestTask.Services;

namespace RatioTestTask.Services
{
    public class StoreService : ServiceBase, IStoreService
    {
        #region Constructors
        public StoreService(ProductContext context) : base(context)
        { }
        #endregion

        #region Methods

        #region public
        public ModelStateDictionary TryAddStore(Store store)
        {
            var modelStateDictionary = this.VerifyStore(store);
            if (modelStateDictionary.ErrorCount != 0)
            {
                return modelStateDictionary;
            }

            this.Context.Stores.Add(store);
            this.Context.SaveChanges();

            return null;
        }

        public IEnumerable<StoreMessageOutput> GetStores()
        {
            var storeMessages = new List<StoreMessageOutput>();
            var storeEntities = this.Context.Stores;

            foreach (var store in storeEntities)
            {
                var storeMessage = this.GetStoreMessage(store);
                storeMessages.Add(storeMessage);
            }

            return storeMessages;
        }

        public StoreMessageOutput TryGetStore(int id)
        {
            var store = this.Context.Stores.AsNoTracking().FirstOrDefault(p => p.StoreId == id);
            if (store == null)
            {
                return null;
            }

            var storeMessage = this.GetStoreMessage(store);
            return storeMessage;
        }
        #endregion

        #region private

        private ModelStateDictionary VerifyStore(Store store)
        {
            var modelStateDictionary = new ModelStateDictionary();

            if (string.IsNullOrEmpty(store.Name))
            {
                modelStateDictionary.AddModelError("Name", $"'Name' cannot has empty or null value");
            }

            if (this.Context.Stores.Any(s => s.Name == store.Name))
            {
                modelStateDictionary.AddModelError("Name", $"Store with name '{store.Name}' already exist");
            }

            return modelStateDictionary;
        }

        private StoreMessageOutput GetStoreMessage(Store store)
        {
            var products = (from product in this.Context.Products
                            from storageItem in product.StorageItems.Where(x => x.StoreId == store.StoreId)
                            select product).ToArray();

            if (products.Length == 0)
            {
                products = null;
            }
            else
            {
                foreach (var product in products)
                {
                    product.StorageItems = null;
                }
            }
            
            var storeMessage = new StoreMessageOutput()
            {
                StoreId = store.StoreId,
                Name = store.Name,
                Products = products
            };

            return storeMessage;
        }
        #endregion

        #endregion
    }
}