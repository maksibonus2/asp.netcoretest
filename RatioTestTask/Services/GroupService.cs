﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TestTask.Message;
using TestTask.Model;
using TestTask.Services.Interfaces;

namespace TestTask.Services
{
    public class GroupService : ServiceBase, IGroupService
    {
        #region Constructors
        public GroupService(ProductContext context) : base(context)
        { }
        #endregion

        #region Methods

        #region public 
        public IEnumerable<GroupMessage> GetGroups()
        {
            var groupMessages = new List<GroupMessage>();
            var groupEntities = this.Context.Groups;

            foreach (var group in groupEntities)
            {
                var groupMessage = this.GetGroupMessage(group);
                groupMessages.Add(groupMessage);
            }

            return groupMessages;
        }

        public ModelStateDictionary TryAddGroup(GroupMessage groupMessage)
        {
            var modelStateDictionary = this.VerifyGroupMessage(groupMessage);
            if (modelStateDictionary.ErrorCount != 0)
            {
                return modelStateDictionary;
            }

            this.CreateNewGroup(groupMessage);

            return null;
        }

        public GroupMessage TryGetGroup(int id)
        {
            var group = this.Context.Groups.AsNoTracking().FirstOrDefault(p => p.GroupId == id);
            if (group == null)
            {
                return null;
            }

            var groupMessage = this.GetGroupMessage(group);
            return groupMessage;
        }
        #endregion

        #region private

        private ModelStateDictionary VerifyGroupMessage(GroupMessage groupMessage)
        {
            var modelStateDictionary = new ModelStateDictionary();

            if (string.IsNullOrEmpty(groupMessage.Name))
            {
                modelStateDictionary.AddModelError("Name", "'Name' cannot has empty or null value");
            }

            if (this.Context.Groups.Any(g => g.Name == groupMessage.Name))
            {
                modelStateDictionary.AddModelError("Name", $"Group with name '{groupMessage.Name}' already exist");
            }

            return modelStateDictionary;
        }

        private GroupMessage GetGroupMessage(Group group)
        {
            var childrenGroups = this.Context.Groups.Where(g => g.ParentId == group.GroupId)
                                                    .Select(g => g.GroupId)
                                                    .ToArray();

            if (childrenGroups.Length == 0)
            {
                childrenGroups = null;
            }

            var groupMessage = new GroupMessage()
            {
                GroupId = group.GroupId,
                Name = group.Name,
                ParentId = group.ParentId,
                ChildrenIds = childrenGroups
            };

            return groupMessage;
        }

        private void CreateNewGroup(GroupMessage groupMessage)
        {
            Group group = new Group()
            {
                Name = groupMessage.Name,
                ParentId = groupMessage.ParentId
            };

            this.Context.Groups.Add(group);
            this.Context.SaveChanges();

            if (groupMessage.ChildrenIds != null)
            {
                foreach (var groupId in groupMessage.ChildrenIds)
                {
                    var childrenGroup = this.Context.Groups.FirstOrDefault(s => s.GroupId == groupId);
                    if (childrenGroup != null)
                    {
                        childrenGroup.ParentId = group.GroupId;
                    }
                }

                this.Context.SaveChanges();
            }
        }
        #endregion

        #endregion
    }
}
