﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using TestTask.Message;

namespace TestTask.Services.Interfaces
{
    public interface IGroupService
    {
        #region Methods
        ModelStateDictionary TryAddGroup(GroupMessage groupMessage);

        GroupMessage TryGetGroup(int id);

        IEnumerable<GroupMessage> GetGroups();
        #endregion
    }
}
