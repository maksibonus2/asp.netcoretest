﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using TestTask.Message;

namespace TestTask.Services.Interfaces
{
    public interface IProductService
    {
        #region Methods
        ModelStateDictionary TryAddProduct(ProductMessageInput productMessage);

        ProductMessageOutput TryGetProduct(int id);

        IEnumerable<ProductMessageOutput> GetProducts();
        #endregion
    }
}
