﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using RatioTestTask.Message;
using System.Collections.Generic;
using TestTask.Model;

namespace RatioTestTask.Services.Interfaces
{
    public interface IStoreService
    {
        #region Methods
        ModelStateDictionary TryAddStore(Store store);

        IEnumerable<StoreMessageOutput> GetStores();

        StoreMessageOutput TryGetStore(int id);

        #endregion
    }
}
