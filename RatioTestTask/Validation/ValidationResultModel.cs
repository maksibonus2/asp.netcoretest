﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace RatioTestTask.Validation
{
    /// <summary>
    /// Code was obtained from
    /// https://www.jerriepelser.com/blog/validation-response-aspnet-core-webapi/
    /// </summary>
    public class ValidationResultModel
    {
        #region Constructors
        public ValidationResultModel(ModelStateDictionary modelState, string fieldNames)
        {
            this.FieldNames = fieldNames;
            this.Message = "Validation Failed.";
            this.Errors = modelState.Keys
                .SelectMany(key => modelState[key].Errors.Select(x => new ValidationError(key, x.ErrorMessage)))
                .ToList();
        }
        #endregion

        #region Properties
        public string FieldNames { get; set; }

        public string Message { get; }

        public List<ValidationError> Errors { get; }
        #endregion

    }
}
