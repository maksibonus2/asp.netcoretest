﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace RatioTestTask.Validation
{
    /// <summary>
    /// Code was obtained from
    /// https://www.jerriepelser.com/blog/validation-response-aspnet-core-webapi/
    /// </summary>
    public class ValidationFailedResult : ObjectResult
    {
        #region Constructors
        public ValidationFailedResult(ModelStateDictionary modelState, string fieldNames = null)
            : base(new ValidationResultModel(modelState, fieldNames))
        {
            this.StatusCode = StatusCodes.Status422UnprocessableEntity;
        }
        #endregion
    }
}
