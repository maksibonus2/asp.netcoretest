﻿using Newtonsoft.Json;

namespace RatioTestTask.Validation
{
    /// <summary>
    /// Code was obtained from
    /// https://www.jerriepelser.com/blog/validation-response-aspnet-core-webapi/
    /// </summary>
    public class ValidationError
    {
        #region Constructors
        public ValidationError(string field, string message)
        {
            this.Field = field != string.Empty ? field : null;
            this.Message = message;
        }
        #endregion

        #region Properties
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Field { get; }

        public string Message { get; }
        #endregion
    }
}
