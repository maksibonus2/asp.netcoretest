﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RatioTestTask.Validation;
using TestTask.Message;
using TestTask.Model;
using TestTask.Services.Interfaces;

namespace TestTask.Constrollers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        #region Constants
        private const string inputFieldNames = "Name, GroupName, Price, VATPrice, VATRate, StoreIds";
        #endregion

        #region Fields
        private readonly ProductContext context;
        private readonly IProductService productService;
        #endregion

        #region Constructors

        public ProductsController(ProductContext context, IProductService productService)
        {
            this.context = context;
            this.productService = productService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public IEnumerable<ProductMessageOutput> GetAll()
        {
            return this.productService.GetProducts();
        }

        [HttpGet("{id}")]
        public ActionResult<ProductMessageOutput> GetProduct(int id)
        {
            var product = this.productService.TryGetProduct(id);
            if (product == null)
            {
                return this.NotFound();
            }

            return product;
        }

        [HttpPost]
        public ActionResult AddProduct([FromBody]ProductMessageInput productMessage)
        {
            var modelStateDictionary = this.productService.TryAddProduct(productMessage);
            if (modelStateDictionary != null)
            {
                return new ValidationFailedResult(modelStateDictionary, inputFieldNames);
            }

            return this.Ok();
        }
        #endregion
    }
}