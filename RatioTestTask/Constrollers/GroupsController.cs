﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RatioTestTask.Validation;
using TestTask.Message;
using TestTask.Model;
using TestTask.Services.Interfaces;

namespace TestTask.Constrollers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        #region Constants
        private const string inputFieldNames = "Name, ParentId, ChildrenIds";
        #endregion

        #region Fields
        private readonly ProductContext context;
        private readonly IGroupService groupService;
        #endregion

        #region Constructors
        public GroupsController(ProductContext context, IGroupService groupService)
        {
            this.context = context;
            this.groupService = groupService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public IEnumerable<GroupMessage> GetGroups()
        {
            return this.groupService.GetGroups();
        }

        [HttpPost]
        public ActionResult AddGroup([FromBody]GroupMessage groupMessage)
        {
            var modelStateDictionary = this.groupService.TryAddGroup(groupMessage);
            if (modelStateDictionary != null)
            {
                return new ValidationFailedResult(modelStateDictionary, inputFieldNames);
            }

            return this.Ok();
        }

        [HttpGet("{id}")]
        public ActionResult<GroupMessage> GetGroup(int id)
        {
            var group = this.groupService.TryGetGroup(id);
            if (group == null)
            {
                return this.NotFound();
            }

            return group;
        }
        #endregion
    }
}
