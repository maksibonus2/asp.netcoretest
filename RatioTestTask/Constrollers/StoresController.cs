﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RatioTestTask.Validation;
using RatioTestTask.Message;
using RatioTestTask.Services.Interfaces;
using TestTask.Model;

namespace RatioTestTask.Constrollers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : ControllerBase
    {
        #region Constants
        private const string inputFieldNames = "Name";
        #endregion

        #region Fields
        private readonly ProductContext context;
        private readonly IStoreService storeService;
        #endregion

        #region Constructors

        public StoresController(ProductContext context, IStoreService storeService)
        {
            this.context = context;
            this.storeService = storeService;
        }
        #endregion

        #region Methods
        [HttpGet]
        public IEnumerable<StoreMessageOutput> GetAll()
        {
            return this.storeService.GetStores();
        }

        [HttpGet("{id}")]
        public ActionResult<StoreMessageOutput> GetStore(int id)
        {
            var store = this.storeService.TryGetStore(id);
            if (store == null)
            {
                return this.NotFound();
            }

            return store;
        }

        [HttpPost]
        public ActionResult AddStore([FromBody]Store store)
        {
            var modelStateDictionary = this.storeService.TryAddStore(store);
            if (modelStateDictionary != null)
            {
                return new ValidationFailedResult(modelStateDictionary, inputFieldNames);
            }

            return this.Ok();
        }
        #endregion
    }
}