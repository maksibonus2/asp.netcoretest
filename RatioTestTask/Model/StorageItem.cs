﻿namespace TestTask.Model
{
    public class StorageItem
    {
        #region Properties
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }

        public int StoreId { get; set; }
        public virtual Store Store { get; set; }
        #endregion
    }
}
