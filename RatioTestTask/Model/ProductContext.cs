﻿using Microsoft.EntityFrameworkCore;

namespace TestTask.Model
{
    public class ProductContext : DbContext
    {
        #region Constructors
        public ProductContext(DbContextOptions<ProductContext> options)
            : base(options)
        { }
        #endregion

        #region Properties
        public DbSet<Product> Products { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<Store> Stores { get; set; }
        #endregion

        #region Methods
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StorageItem>()
                .HasKey(item => new { item.ProductId, item.StoreId });
        }
        #endregion
    }
}
