# ASP.NETCoreTest
Project use ASP.NET Core 2.2.

Run project using Kestrel web server (not "IIS Express") by selecting "RatioTestTask" in start button.
 
If you want to create an *EMPTY* database, then before launching the project in Visual Studio for the first time, go to the Package Manage Console window and run the following command:
```
Update-database
```
 
If you want to create a database with initial data, then just run the project. A database with initialized values will be created automatically.